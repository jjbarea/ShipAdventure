package cat.xtec.ioc.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;

import cat.xtec.ioc.helpers.AssetManager;

import static com.badlogic.gdx.graphics.g2d.Animation.PlayMode;

public class Moneda extends Actor{
    public static final int MONEDA_ANCHO=30;
    public static final int MONEDA_ALTO=30;
    private Vector3 position;
    private Circle topes;
    public boolean cojida;

    public boolean sonar;
    // Moneda
    public static Animation monedaAnim;


    public Moneda(float x,float y) {
        this.position = new Vector3(x,y,0);

        //Creem l'animació
       monedaAnim = AssetManager.monedaAnim;
       monedaAnim.setPlayMode(PlayMode.LOOP);

        //Creamos las fisicas de la monedaç
        this.topes = new Circle(x,y,MONEDA_ANCHO/2) ;


        //Boolean de que la nave no esta cojida
        this.cojida = false;
        //Boolean para sonar
        this.sonar=true;
    }

    public Vector3 getPosition() {
        return position;
    }

    public Circle getTopes() {
        return topes;
    }


    public static Animation getMonedaAnim() {
        return monedaAnim;
    }

    //Comprobamos si el jugador ha cojido la moneda
    public boolean choque(Rectangle jugador){
        if(Intersector.overlaps(topes,jugador)){
            AssetManager.sonidoMoneda.play(0.2f);
            sonar=false;
            return true;
        }else {
            return false;
        }
    }

    public void reposicionar(float x,float y){
        this.position.x=x;
        this.position.y=y;
        topes.setPosition(x,y);
    }


}
