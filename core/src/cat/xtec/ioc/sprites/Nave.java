package cat.xtec.ioc.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;

import cat.xtec.ioc.helpers.AssetManager;


public class Nave extends Actor {
    public static int GRAVEDAD = -15;
    public static final int NAVE_ANCHO = 50;
    public static final int NAVE_ALTO=30;
    public static int MOVIMIENTO=120;
    private Vector3 position;
    private Vector3 velocity;
    private Rectangle topes;
    public boolean choque;
    public boolean pausa;

    private Texture nave;

    public Nave(int x, int y){
        position=new Vector3(x,y,0);
        velocity=new Vector3(0,0,0);

        //Creamos las texturas
       nave=AssetManager.nave;
       topes=new Rectangle(x,y+2,NAVE_ANCHO-5,NAVE_ALTO-7);

        choque=false;
        pausa=false;
    }


    public void update(float delta){


        if(!pausa) {

            velocity.add(0, GRAVEDAD, 0);
            velocity.scl(delta);
            if (!choque) {
                position.add(MOVIMIENTO * delta, velocity.y, 0);
            }

            if (position.y < 0) {
                position.y = 0;
            }
            velocity.scl(1 / delta);
            updateTopes();
        }
    }

    public void updateTopes(){
        topes.setPosition(position.x,position.y);
    }

    public Vector3 getPosition() {
        return position;
    }


    public Texture getNave() {
        return nave;
    }

    public void salto(){
        velocity.y=250;
        //efecto.play(0.5f);
    }

    public Rectangle getLimites(){
        return topes;
    }

    public void dispose(){
        nave.dispose();

    }


    public boolean sonidoExplosion(){
        AssetManager.explosionSound.play(0.5f);
        return false;
    }


}
