package cat.xtec.ioc.sprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.Random;

import javax.xml.soap.Text;

import cat.xtec.ioc.helpers.AssetManager;

public class Tubo extends Actor{

    private static final int FLUCTUACION = 40;
    private static final int ESPACIO_TUBO=85;
    public static final int ANCHO_TUBO=15;
    private Rectangle topeArriba,topeAbajo;
    private static final int ESPACIO_MINIMO=20;
    private TextureRegion tuboArriba,tuboAbajo;
    private Vector2 posTuboArriba,posTuboAbajo;
    private Random rand;

    public Tubo(float x){


        //Cargamos las texturas de los tubos
        tuboAbajo= AssetManager.tuboAbajo;
        tuboArriba=AssetManager.tuboArriba;

        rand = new Random();

        //Les damos posicion a los tubos
        posTuboArriba = new Vector2(x,rand.nextInt(FLUCTUACION)+ ESPACIO_TUBO+ESPACIO_MINIMO);
        posTuboAbajo = new Vector2(x,posTuboArriba.y - ESPACIO_TUBO-tuboAbajo.getRegionHeight());

        //Creamos los limites de los tubos
        topeArriba = new Rectangle(posTuboArriba.x,posTuboArriba.y,tuboArriba.getRegionWidth(),tuboArriba.getRegionHeight());
        topeAbajo = new Rectangle(posTuboAbajo.x,posTuboAbajo.y,tuboAbajo.getRegionWidth(),tuboAbajo.getRegionHeight());


    }

    public TextureRegion getTuboArriba() {
        return tuboArriba;
    }



    public TextureRegion getTuboAbajo() {
        return tuboAbajo;
    }


    public Vector2 getPosTuboArriba() {
        return posTuboArriba;
    }



    public Vector2 getPosTuboAbajo() {
        return posTuboAbajo;
    }

    //Colocamos el tubo al final de la pantalla
    public void reposicionar(float x){
        //Texturas
        posTuboArriba.set(x,rand.nextInt(FLUCTUACION)+ ESPACIO_TUBO+ESPACIO_MINIMO);
        posTuboAbajo.set(x,posTuboArriba.y - ESPACIO_TUBO-tuboAbajo.getRegionHeight());
        //Topes
        topeArriba.set(posTuboArriba.x,posTuboArriba.y,tuboArriba.getRegionWidth(),tuboArriba.getRegionHeight());
        topeAbajo.set(posTuboAbajo.x,posTuboAbajo.y,tuboAbajo.getRegionWidth(),tuboAbajo.getRegionHeight());
    }

    //Comprobamos si el jugador ha chocado con un tubo
    public boolean choque(Rectangle jugador){

        return jugador.overlaps(topeArriba) || jugador.overlaps(topeAbajo);
    }

    public void dispose(){
        //tuboArriba.dispose();
        //tuboAbajo.dispose();


    }



}
