package cat.xtec.ioc;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Map;

import javax.naming.Context;

import cat.xtec.ioc.sprites.Nave;
import cat.xtec.ioc.states.GameStateManager;
import cat.xtec.ioc.states.MenuState;
import cat.xtec.ioc.helpers.AssetManager;


public class ShipAdventure extends ApplicationAdapter {
public static final int GAME_WIDTH=600;
public static final int GAME_HEIGHT=300;
public static Preferences preferencias;
public static final String TITLE="ShipAdventure Game";

private GameStateManager gam;
private SpriteBatch batch;

private static Music musica;

    public ShipAdventure() {



    }
    @Override
    public void dispose() {
        super.dispose();
        musica.dispose();
    }

    @Override
    public void create() {
        preferencias=Gdx.app.getPreferences("Preferencies.prefs");
        // A l'iniciar el joc carreguem els recursos
        AssetManager.load(0);
        // I definim la pantalla d'splash com a pantalla
        batch = new SpriteBatch();
        gam=new GameStateManager();
        //Creem la musica del joc
        musica = AssetManager.musicPantalla;
        //Posem la musica en bucle
        musica.setLooping(true);
        //Definidm el volum
        musica.setVolume(0.1f);

        //Quan arrenci el joc comenci a sonar
        musica.play();

        Gdx.gl.glClearColor(1,0,0,1);
        //Cridem a la pantalla del menu
        gam.push(new MenuState(gam,0));

    }


   public void render(){

       Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
       gam.update(Gdx.graphics.getDeltaTime());
       gam.render(batch);

    }


    public Preferences getPreferencias() {
        return preferencias;
    }

    public static void bajarVolumen(){
        musica.setVolume(0.0f);
    }
    public static void subirVolumen(){
        musica.setVolume(0.2f);
    }
}