package cat.xtec.ioc.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import javax.xml.soap.Text;

import static com.badlogic.gdx.graphics.g2d.Animation.PlayMode.LOOP;


public class AssetManager {

    // Sprite de la nau
    public static Texture nave;

    //fondos
    public static Texture fondoJuego,fondoMenus;

    //Tubos
    public static TextureRegion tuboArriba,tuboAbajo;

    //Escudo
    public static Texture escudo;

    //FinJuego
    public static Texture finJuego;

    // Moneda
    public static TextureRegion[] monedas;
    public static Animation monedaAnim;

    // Sheet monedas,explosio
    public static Texture hojaMonedas,hojaExplosion;

    //Suelo
    public static Texture suelo;
    // Explosió
    public static TextureRegion[] explosion;
    public static Animation explosionAnim;

    //Array corazones
    public static Texture corazon;

    // Sons
    public static Sound explosionSound;
    public static Sound sonidoMoneda;
    public static Music musicJuego,musicPantalla;
    public static Sound aplausos,abucheo,sonidoBoton,revivir;

    // Font
    public static BitmapFont font,font1;

    //Simbolos Ready
    public static Texture leftTap,rightTap,cursor,getReady;

    //Record
    public static Texture record;



    public static void load(int idNave) {


        /*******************************Ships & Background*****************************************/


        switch (idNave){
            case 0:
                nave=new Texture(Gdx.files.internal("fly.png"));
                fondoJuego=new Texture(Gdx.files.internal("bg4.png"));
                break;
            case 1:
                nave=new Texture(Gdx.files.internal("spaceship01.png"));
                fondoJuego=new Texture(Gdx.files.internal("bg1.png"));
                break;
            case 2:
                nave=new Texture(Gdx.files.internal("spaceship02.png"));
                fondoJuego=new Texture(Gdx.files.internal("bg2.png"));
                break;
            case 3:
                nave=new Texture(Gdx.files.internal("ufo.png"));
                fondoJuego=new Texture(Gdx.files.internal("bg3.png"));
                break;

        }

        fondoMenus=new Texture(Gdx.files.internal("bg5.jpg"));

        /******************************* Escudo *************************************/

        escudo=new Texture(Gdx.files.internal("spr_shield.png"));
        //Sonido revivir
        revivir=Gdx.audio.newSound(Gdx.files.internal("sounds/reviveSound.mp3"));

        /********************************Tubos***************************************/

        tuboArriba = new TextureRegion(new Texture(Gdx.files.internal("tuboGris.png")));
        tuboArriba.flip(true,true);
        tuboAbajo = new TextureRegion(new Texture(Gdx.files.internal("tuboGris.png")));


        /****************************** Moneda **********************************/

        hojaMonedas = new Texture(Gdx.files.internal("monedas.png"));

        monedas=new TextureRegion[6];
        //Carreguem monedes
        int index = 0;
        for (int i = 0; i < 1; i++) {
            for (int j = 0; j < 6; j++) {

                monedas[index++] = new TextureRegion(hojaMonedas, j * 120, 0, 104, 200);
                monedas[index-1].flip(false, true);
            }
        }


        // Finalment creem l'animació
        monedaAnim = new Animation(0.4f, monedas);

        monedaAnim.setPlayMode(LOOP);

        /****************************** Record ****************************************/

        record=new Texture(Gdx.files.internal("fbs-21.png"));


        /******************************* Suelo ***************************************/

        suelo = new Texture("suelo.png");

        /****************************** Corazon **************************************/

        corazon=new Texture(Gdx.files.internal("corazon.png"));

        /******************************* Explosio ***********************************/

        // Creem els 16 estats de l'explosió
        //explosion = new TextureRegion[25];
        explosion = new TextureRegion[64];

        // Carreguem les textures i li apliquem el mètode d'escalat 'nearest'
        //hojaExplosion = new Texture(Gdx.files.internal("explosion.png"));
        hojaExplosion = new Texture(Gdx.files.internal("explosion1.png"));

        // Carreguem els 16 estats de l'explosió
        index = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                //explosion[index++] = new TextureRegion(hojaExplosion,  j * 64, i*64, 64, 64);
                //explosion[index-1].flip(false, true);

                explosion[index++] = new TextureRegion(hojaExplosion,  j * 512, i*512, 512, 512);
                explosion[index-1].flip(false, true);
            }
        }


        // Finalment creem l'animació
        explosionAnim = new Animation(0.4f, explosion);

        //finJuego
        finJuego=new Texture(Gdx.files.internal("gameover.png"));

        /******************************* Sounds *************************************/
        // Explosió
        explosionSound = Gdx.audio.newSound(Gdx.files.internal("sounds/Explosion+2.mp3"));

        //Moeda
        sonidoMoneda = Gdx.audio.newSound(Gdx.files.internal("sounds/sonidoMoneda.mp3"));

        //Aplausos
        aplausos = Gdx.audio.newSound(Gdx.files.internal("sounds/applause7.mp3"));
        //Abucheo
        abucheo = Gdx.audio.newSound(Gdx.files.internal("sounds/boo3.mp3"));

        // Música del joc
        musicJuego = Gdx.audio.newMusic(Gdx.files.internal("sounds/hotpursuit.mp3"));
        musicJuego.setVolume(0.2f);
        musicJuego.setLooping(true);
        //Musica de les pantalles
        musicPantalla=Gdx.audio.newMusic(Gdx.files.internal("sounds/Dreamer.mp3"));
        musicPantalla.setVolume(0.2f);
        musicPantalla.setLooping(true);
        //Sonido boton
        sonidoBoton=Gdx.audio.newSound(Gdx.files.internal("sounds/button-19.mp3"));

        /******************************* Ready Simbols *****************************/

        leftTap=new Texture(Gdx.files.internal("fbs-30.png"));
        rightTap=new Texture(Gdx.files.internal("fbs-29.png"));
        cursor=new Texture(Gdx.files.internal("fbs-28.png"));
        getReady=new Texture(Gdx.files.internal("fbs-31.png"));

        /******************************* Text *************************************/
        // Font space
        //final String FONT_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@|\\/?-+=()*&.;,{}\"´`'<>";
        FileHandle fontFile = Gdx.files.internal("fonts/FuentaDefinitiva-export.fnt");
        FileHandle fontFile1 = Gdx.files.internal("fonts/FuentaDefinitiva-export.fnt");

        //Fuentes
        font = new BitmapFont(fontFile,false);
        font1=new BitmapFont(fontFile1,false);
        font.setColor(Color.ORANGE);
        font.getData().setScale(0.3f);
        font1.getData().setScale(0.2f);


    }

    public static void dispose() {

        // Descrtem els recursos

        System.out.println("Asset Manager disposed");
        fondoJuego.dispose();
        fondoMenus.dispose();
        corazon.dispose();
        hojaExplosion.dispose();
        hojaMonedas.dispose();
        suelo.dispose();
        explosionSound.dispose();
        musicJuego.dispose();
        record.dispose();
        leftTap.dispose();
        rightTap.dispose();
        cursor.dispose();
        explosionSound.dispose();
        sonidoBoton.dispose();
        sonidoMoneda.dispose();
        revivir.dispose();

        //musicPantalla.dispose();

    }
}
