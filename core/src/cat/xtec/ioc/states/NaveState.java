package cat.xtec.ioc.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import javax.print.attribute.standard.MediaSize;

import cat.xtec.ioc.ShipAdventure;
import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.sprites.Nave;

public class NaveState extends State {
    //Stage
    protected Stage stage;
    //Textura fons
    protected Texture fons;
    //ImageButton de les naus
    protected Image aeroNave,naveDino1,naveDino2,ufo;

    //label del titol
    protected Label selNave,ae,nd1,nd2,ovni;
    protected Label.LabelStyle labelStyle;
    protected Label.LabelStyle labelStyle1;


    public NaveState(final GameStateManager gam) {
        super(gam);

        // Creem el viewport amb les mateixes dimensions que la càmera
        StretchViewport viewport = new StretchViewport(ShipAdventure.GAME_WIDTH/2,ShipAdventure.GAME_HEIGHT/2,cam);

        //Creem l'stage i assignem el viewport
        stage = new Stage(viewport);

        //Creem la textura del fons
        fons=AssetManager.fondoMenus;


        //Creem les image de les naus;
        aeroNave=new Image( new Texture("fly.png"));
        aeroNave.setBounds(cam.position.x/2-(Nave.NAVE_ANCHO+10)/2,cam.position.y-10, Nave.NAVE_ANCHO+10,Nave.NAVE_ALTO+10);

        naveDino1=new Image(new Texture("spaceship01.png"));
        naveDino1.setBounds(cam.position.x*1.5f-Nave.NAVE_ANCHO+10/2,cam.position.y-10,Nave.NAVE_ANCHO+10,Nave.NAVE_ALTO+10);

        naveDino2=new Image(new Texture("spaceship02.png"));
        naveDino2.setBounds(cam.position.x/2-(Nave.NAVE_ANCHO+10)/2,cam.position.y-65,Nave.NAVE_ANCHO+10,Nave.NAVE_ALTO+10);

        ufo=new Image(new Texture("ufo.png"));
        ufo.setBounds(cam.position.x*1.5f-Nave.NAVE_ANCHO+10/2,cam.position.y-65,Nave.NAVE_ANCHO+10,Nave.NAVE_ALTO+10);


        //Afegim els listeners de les naus
        aeroNave.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AssetManager.dispose();
                AssetManager.load(0);
                AssetManager.sonidoBoton.play(0.2f);
                gam.set(new MenuState(gam,0));
            }
        });

        naveDino1.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AssetManager.dispose();
                AssetManager.load(1);
                AssetManager.sonidoBoton.play(0.2f);
                gam.set(new MenuState(gam,1));
            }
        });

        naveDino2.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AssetManager.dispose();
                AssetManager.load(2);
                AssetManager.sonidoBoton.play(0.2f);
                gam.set(new MenuState(gam,2));
            }
        });

        ufo.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AssetManager.dispose();
                AssetManager.load(3);
                AssetManager.sonidoBoton.play(0.2f);
                gam.set(new MenuState(gam,3));
            }
        });



        //Creem el label de seleccionar nave
        labelStyle=new Label.LabelStyle();
        labelStyle.font= AssetManager.font;
        selNave=new Label("SELECCIONAR NAVE",labelStyle);
        selNave.setBounds(cam.viewportWidth/2-selNave.getWidth()/2,cam.viewportHeight-selNave.getHeight(),selNave.getWidth(),selNave.getHeight());


        //Ver donde clico

        //Creem els labels de la dificultad
        labelStyle1=new Label.LabelStyle();
        labelStyle1.font=AssetManager.font1;
        ae=new Label("AVIONETA",labelStyle1);
        System.out.println(aeroNave.getImageX());
        ae.setBounds(cam.position.x/2-(Nave.NAVE_ANCHO+10)/2+5,cam.position.y+35,ae.getWidth(),ae.getHeight());

        nd1=new Label("REX",labelStyle1);
        nd1.setBounds(cam.position.x*1.5f-(Nave.NAVE_ANCHO+10)/2+3,cam.position.y+35,nd1.getWidth(),nd1.getHeight());

        nd2=new Label("SHARK",labelStyle1);
        nd2.setBounds(cam.position.x/2-(Nave.NAVE_ANCHO+10)/2+10,cam.position.y-25,nd2.getWidth(),nd2.getHeight());

        ovni=new Label("OVNI",labelStyle1);
        ovni.setBounds(cam.position.x*1.5f-(Nave.NAVE_ANCHO+10)/2+3,cam.position.y-25,ovni.getWidth(),ovni.getHeight());

        //Afegim el fons al stage
        Image fonsImage=new Image(fons);
        fonsImage.setBounds(cam.position.x-(cam.viewportWidth/2),0,cam.viewportWidth,cam.viewportHeight);
        stage.addActor(fonsImage);
        //Afegim les naus
        stage.addActor(aeroNave);
        stage.addActor(naveDino1);
        stage.addActor(naveDino2);
        stage.addActor(ufo);
        //Afegim el label de seleccionar nau
        stage.addActor(selNave);
        //Afegim els labels de dificultad
        stage.addActor(ae);
        stage.addActor(nd1);
        stage.addActor(nd2);
        stage.addActor(ovni);

        Gdx.input.setInputProcessor(stage);


    }

    @Override
    protected void handleInput() {

    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch sb) {

        stage.draw();
    }

}
