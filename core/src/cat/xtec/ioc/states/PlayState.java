package cat.xtec.ioc.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;


import org.omg.PortableServer.LifespanPolicyOperations;

import java.util.Random;

import cat.xtec.ioc.ShipAdventure;
import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.sprites.Moneda;
import cat.xtec.ioc.sprites.Nave;
import cat.xtec.ioc.sprites.Tubo;

public class PlayState extends State {

    private static final int ESPACIO_TUBO = 125;
    private static final int CONTADOR_TUBOS = 4;
    private static final int SUELO_ABAJO = -100;
    //Nave
    private Nave nave;
    //Fondo
    private Texture bg;
    //Suelo
    private Texture suelo;
    private Vector2 suelo1, suelo2;
    //Puntuacion
    private int puntuacion;
    private long puntuacionMax;

    //boolean para salir a puntuacion
    private boolean juegoAcabado;

    //Tubos
    private Array<Tubo> tubos;

    //Stage
    protected Stage stage;

    //Identificador de la configuracion
    private static int idConf;


    // Explosió
    public static Animation explosionAnim;
    public static float tiempoAnim;

    //controlar sonido
    private boolean sonar;

    //boolean de pausa
    private static boolean pausa;

    // Per controlar l'animació de l'explosió
    private float explosionTime = 0;

    //Aleatori per posicionar la moneda
    Random rand;

    //Moneda
    private Moneda moneda;

    //Contador juego acabado
    private int cont;
    //booelean ready
    private boolean ready;
    //vidas
    private int vidas;


    protected PlayState(GameStateManager gam, int puntuacion, int idConf, int vidas) {


        super(gam);
        rand = new Random();

        this.idConf = idConf;
        if (puntuacion > 0) {
            AssetManager.revivir.play(0.25f);
        }

        //Creamos el objectos preferencias
        Preferences prefs = ShipAdventure.preferencias;

        puntuacionMax = prefs.getLong("max");

        // Creem el viewport amb les mateixes dimensions que la càmera
        StretchViewport viewport = new StretchViewport(ShipAdventure.GAME_WIDTH / 2, ShipAdventure.GAME_HEIGHT / 2, cam);

        //Creem l'stage i assignem el viewport
        stage = new Stage(viewport);


        //Creem l'objecte nau
        nave = new Nave(20, 75);
        //Paramos a la nave para que no caiga hasta que empecemos a jguar
        nave.pausa = true;

        //Establim le dimensions de la camara
        cam.setToOrtho(false, ShipAdventure.GAME_WIDTH / 2, ShipAdventure.GAME_HEIGHT / 2);


        //Creamos la textura del fondo
        bg = AssetManager.fondoJuego;

        //Bajamos a 0 el volumen de la musica de pantallas
        ShipAdventure.bajarVolumen();

        if (puntuacion == 0) {
            AssetManager.musicJuego.play();
        }

        //Creamos las texturas y fisicas del suelo
        suelo = AssetManager.suelo;
        suelo1 = new Vector2(cam.position.x - cam.viewportWidth / 2 - 30, SUELO_ABAJO);
        suelo2 = new Vector2((cam.position.x - cam.viewportWidth / 2) - 30 + suelo.getWidth(), SUELO_ABAJO);

        //Puntuacion
        this.puntuacion = puntuacion;

        //Creamos el array de tubos
        tubos = new Array<Tubo>();

        //Añadimos 4 tubos al array de tubos
        for (int i = 1; i <= CONTADOR_TUBOS; i++) {
            tubos.add(new Tubo((i + 1) * (ESPACIO_TUBO + Tubo.ANCHO_TUBO)));
        }

        //Creem l'objecte moneda en el espacio enrtre tubo 1 y tubo 2 por 4 para que no salga en pantalla
        moneda = new Moneda(3 * ((tubos.get(0).getPosTuboAbajo().x) + Tubo.ANCHO_TUBO + ESPACIO_TUBO / 2) - Moneda.MONEDA_ANCHO / 2, rand.nextInt(100) + 30);

        //Boolean para controlar el final del juego
        juegoAcabado = false;

        // Creem l'animacio de  l'explosio
        explosionAnim = AssetManager.explosionAnim;
        explosionTime = 0;


        //La iniciamos a true para que suene una vez la explosion
        sonar = true;


        //Controlamos si el juego esta en pausa
        pausa = false;

        //Contador para esperar unos segundos cuando el juego acabe
        cont = 0;

        //Contador para empezar a jugar
        ready = false;

        //Ponemos las vidas  1
        this.vidas = vidas;


        Gdx.input.setInputProcessor(stage);

    }

    @Override
    protected void handleInput() {

        if (cont >= 40) ready = true;

        if (Gdx.input.justTouched() && ready) {
            nave.pausa=false;

            if (juegoAcabado) {
                if (cont >= 90) {
                    AssetManager.musicJuego.stop();
                    gam.set(new PuntuacionState(gam, vidas, puntuacion, idConf));
                }
            } else {
                nave.salto();
            }
        }
    }


    @Override
    public void update(float dt) {

        handleInput();
        updateSuelo();
        nave.update(dt);
        cam.position.x = nave.getPosition().x + 100;


        for (Tubo tubo : tubos) {
            if (cam.position.x - (cam.viewportWidth / 2) > tubo.getPosTuboArriba().x + tubo.getTuboArriba().getRegionWidth()) {
                tubo.reposicionar(tubo.getPosTuboArriba().x + ((Tubo.ANCHO_TUBO + ESPACIO_TUBO) * CONTADOR_TUBOS));
            }

            //Si la nave choca con un tubo
            if (tubo.choque(nave.getLimites())) {
                nave.choque = true;
                if (vidas == 1) {
                    juegoAcabado = true;
                } else {
                    vidas--;
                    gam.set(new PlayState(gam, puntuacion, idConf, vidas));

                }

            }

        }

        //Si la moneda sale de la camara o la cojemos reposicionamos
        if (cam.position.x - (cam.viewportWidth / 2) > moneda.getPosition().x + moneda.MONEDA_ANCHO + 10 || moneda.cojida) {
            int i = rand.nextInt(100) + 30;
            moneda.reposicionar(moneda.getPosition().x + ((Tubo.ANCHO_TUBO + ESPACIO_TUBO) * CONTADOR_TUBOS), i);
            moneda.cojida = false;

        }

        //Si la nave no choca y no estmaos en pausa
        if (nave.choque != true && !nave.pausa)
            puntuacion++;

        //Si "chocamos" con una moneda
        if (moneda.choque(nave.getLimites())) {
            moneda.cojida = true;
            if (vidas <= 2) vidas++;
        }

        //Si la nave toca el suelo
        if (nave.getPosition().y <= suelo.getHeight() + SUELO_ABAJO) {
            nave.choque = true;
            if (vidas == 1) {
                juegoAcabado = true;
            } else {
                vidas--;
                gam.set(new PlayState(gam, puntuacion, idConf, vidas));
            }


        }

        //Aumantamos la variable contador
        if (juegoAcabado) cont++;

        if (!ready) cont++;

        cam.update();


    }

    @Override
    public void render(SpriteBatch sb) {


        sb.setProjectionMatrix(cam.combined);
        sb.begin();


        //Dibujamos el fondo
        sb.draw(bg, cam.position.x - (cam.viewportWidth / 2), 0, cam.viewportWidth, cam.viewportHeight);


        if (puntuacion == 0) {
            //System.out.println("Dibujando en "+(cam.position.x-40+","+(cam.position.y-30)));

            sb.draw(AssetManager.leftTap, cam.position.x - 25, cam.position.y - 20, 15, 12);
            sb.draw(AssetManager.rightTap, cam.position.x + 11, cam.position.y - 20, 15, 12);
            sb.draw(AssetManager.cursor, cam.position.x - 8, cam.position.y - 20, 15, 20);
            sb.draw(AssetManager.getReady, cam.position.x - 150 / 2, cam.position.y - 65, 150, 40);

        }

        //Dibujamos la nave
        if (!nave.choque)
            sb.draw(nave.getNave(), nave.getPosition().x, nave.getPosition().y, nave.NAVE_ANCHO / 1.4f, nave.NAVE_ALTO / 1.2f);

        //Dibujamos el esuco
        if (vidas > 1) {
            sb.draw(AssetManager.escudo, nave.getPosition().x - 10, nave.getPosition().y - 10, nave.NAVE_ANCHO + 10, nave.NAVE_ALTO + 20);
        }

        //Dibujamos los tubos
        for (Tubo tubo : tubos) {
            sb.draw(tubo.getTuboArriba(), tubo.getPosTuboArriba().x, tubo.getPosTuboArriba().y);
            sb.draw(tubo.getTuboAbajo(), tubo.getPosTuboAbajo().x, tubo.getPosTuboAbajo().y);
        }


        //Colocamos la puntuacion
        AssetManager.font.draw(sb, puntuacion + "", cam.position.x - (cam.viewportWidth / 2) + 5, cam.viewportHeight - 5);
        if (puntuacion > puntuacionMax) {

            sb.draw(AssetManager.record, cam.position.x - (cam.viewportWidth / 2) + 5, cam.viewportHeight - 30, 17, 10);
        }
        //Dibujamos el suelo
        sb.draw(suelo, suelo1.x, suelo1.y);
        sb.draw(suelo, suelo2.x, suelo2.y);

        //Si ha acabado el juego pintamos un GameOver
        if (juegoAcabado) {

            // Si hi ha hagut col·lisió: Reproduïm l'explosió
            if (sonar) {
                sonar = nave.sonidoExplosion();
            }
            sb.draw(explosionAnim.getKeyFrame(explosionTime, false), nave.getPosition().x - 1.5f * (nave.NAVE_ANCHO), nave.getPosition().y - 73, 200, 200);

            explosionTime += Gdx.graphics.getRawDeltaTime() + 0.135f;
            sb.draw(AssetManager.finJuego, cam.position.x - AssetManager.finJuego.getWidth() / 2, cam.position.y - 50);
        }

        //Pintamos la animacion de la moneda
        sb.draw(moneda.getMonedaAnim().getKeyFrame(tiempoAnim, false), moneda.getPosition().x, moneda.getPosition().y, moneda.MONEDA_ANCHO, moneda.MONEDA_ALTO);
        tiempoAnim += Gdx.graphics.getRawDeltaTime() + 0.01f;

        //Pintamos los corazones
        for (int i = 0; i < vidas; i++) {
            float x = cam.position.x + (cam.viewportWidth / 2) - 20 - (i * 20);
            sb.draw(AssetManager.corazon, x, cam.viewportHeight - 18, 15, 15);
        }

        if (!ready && puntuacion != 0) {
            AssetManager.font.draw(sb, "HAS PERDIDO UNA VIDA", cam.position.x / 2 - 35, cam.position.y);
        }


        //AssetManager.font.draw(sb,Gdx.graphics.getFramesPerSecond()+"",cam.position.x,cam.viewportHeight-30);
        Gdx.graphics.setTitle("FPS:" + Gdx.graphics.getFramesPerSecond());
        sb.end();

    }


    private void updateSuelo() {
        if (cam.position.x - (cam.viewportWidth / 2) > suelo1.x + suelo.getWidth()) {
            suelo1.add(suelo.getWidth() * 2, 0);
        }
        if (cam.position.x - (cam.viewportWidth / 2) > suelo2.x + suelo.getWidth()) {
            suelo2.add(suelo.getWidth() * 2, 0);
        }
    }


}
