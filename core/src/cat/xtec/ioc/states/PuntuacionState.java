package cat.xtec.ioc.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import javax.xml.soap.Text;

import cat.xtec.ioc.ShipAdventure;
import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.sprites.Nave;

/**
 * Created by Joel on 30/05/2018.
 */

public class PuntuacionState extends State {

    //Fondo
    private Texture fondo;

    private Image playButton;
    private Image homeButton;
    private Image exitButton;

    //Puntos
    private int puntuacion;
    private long punuacionMax;
    //Stage
    protected Stage stage;
    //Punts
    protected Label punts;
    protected Label.LabelStyle puntsStyle;
    //Boton volver menu
    protected TextButton menu;
    protected TextButton.TextButtonStyle buttonStyle;
    //Boton volver jugar
    protected TextButton jugar;
    //Punts
    protected Label puntsMax;
    protected Label felicitacion;
    protected Label lastima;

    public PuntuacionState(final GameStateManager gam, final int vidas, int puntuacion, final int idConf) {

        super(gam);

        // Creem el viewport amb les mateixes dimensions que la càmera
        StretchViewport viewport = new StretchViewport(ShipAdventure.GAME_WIDTH/2,ShipAdventure.GAME_HEIGHT/2,cam);

        //Creem l'stage i assignem el viewport
        stage = new Stage(viewport);

        //Asignamos el fondo
        fondo=AssetManager.fondoJuego;

        //Cremos el playbtn, el homebutton y el salir y les damos posicion
        playButton=new Image(new Texture(("play.png")));
        playButton.setBounds(40,20,80,35);
        homeButton=new Image(new Texture(("fbs-17.png")));
        homeButton.setBounds(170,23,60,30);
        exitButton=new Image(new Texture("cruz.png"));
        exitButton.setBounds(cam.viewportWidth-20,0,20,20);

        //Añadimos los listeners
        homeButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AssetManager.sonidoBoton.play(0.2f);
                AssetManager.load(idConf);
                gam.set(new MenuState(gam,idConf));
            }
        });
        playButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AssetManager.sonidoBoton.play(0.2f);
                AssetManager.load(idConf);
                gam.set(new PlayState(gam,0,idConf,vidas));
            }
        });
        exitButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AssetManager.sonidoBoton.play(0.2f);
                Gdx.app.exit();
            }
        });

        //Subimos el volumen de la musica de pantallas
        ShipAdventure.subirVolumen();

        //Creamos el objectos preferencias
        Preferences prefs=ShipAdventure.preferencias;


        //Cogemos la puntuacion maxima del fichero de preferencias
        punuacionMax=prefs.getLong("max");

        //Si la puntuacion obtenida es mayor que la maxima, guardamos la nueva maxima
        if(puntuacion>=punuacionMax){

            AssetManager.aplausos.play(0.3f);
            prefs.clear();
            prefs.putLong("max",puntuacion);
            prefs.flush();
            punuacionMax=puntuacion;

        }else{
            AssetManager.abucheo.play(0.3f);

        }


        //Creem el puntsStyle
        puntsStyle=new Label.LabelStyle();
        //Assignem la font
        puntsStyle.font=AssetManager.font;
        //Creem el label de punts
        punts=new Label("PUNTUACION ACTUAL: "+puntuacion,puntsStyle);
        puntsMax=new Label("PUNTUACION MAXIMA: "+punuacionMax,puntsStyle);
        //Coloquem la puntuacio
        punts.setBounds(25,80,punts.getWidth(),punts.getHeight());
        puntsMax.setBounds(25,60,puntsMax.getWidth(),puntsMax.getHeight());
        //Creem el label de felicitació/llastima
        felicitacion=new Label("FELICIDADES!, HAS\nSUPERADO LA PUNTUACION\nMAXIMA!",puntsStyle);
        lastima=new Label("LASTIMA, INTENTALO \nOTRA VEZ!",puntsStyle);
        //Coloquem felicitacio/lastima
        felicitacion.setBounds(25, 80,felicitacion.getWidth(), felicitacion.getHeight());
        lastima.setBounds(25, 90,felicitacion.getWidth(), felicitacion.getHeight());

        //Afegim el fons al stage
        Image fons=new Image(fondo);
        fons.setBounds(cam.position.x-(cam.viewportWidth/2),0,cam.viewportWidth,cam.viewportHeight);
        stage.addActor(fons);
        //Afegim la puntuacio
        if(puntuacion>=punuacionMax) {
            stage.addActor(felicitacion);
            stage.addActor(puntsMax);
        }
        else {
            stage.addActor(lastima);
            stage.addActor(punts);
            stage.addActor(puntsMax);
        }
        //Afegim els botons
        stage.addActor(playButton);
        stage.addActor(homeButton);
        stage.addActor(exitButton);

        Gdx.input.setInputProcessor(stage);

        this.puntuacion=puntuacion;
    }


    @Override
    protected void handleInput() {




    }

    @Override
    public void update(float dt) {

        handleInput();

        cam.update();
    }

    @Override
    public void render(SpriteBatch sb) {



        stage.draw();

       // sb.begin();

        //sb.draw(fondo,0,0, ShipAdventure.GAME_WIDTH,ShipAdventure.GAME_HEIGHT);
        //AssetManager.font.draw(sb,puntuacion+"",cam.position.x-(cam.viewportWidth/2)+10,cam.viewportHeight-10);

        //sb.end();

    }

}
