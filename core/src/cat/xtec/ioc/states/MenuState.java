package cat.xtec.ioc.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import javax.swing.MutableComboBoxModel;

import cat.xtec.ioc.ShipAdventure;
import cat.xtec.ioc.helpers.AssetManager;
import cat.xtec.ioc.sprites.Nave;

public class MenuState extends State {

    //TExtura del fondo
    private Texture fons;

    //Stage
    protected Stage stage;

    //textbuttonStyle
    protected TextButton.TextButtonStyle buttonStyle;

    //Boton de jugar
    protected TextButton jugar;

    //Boton de Naves
    protected TextButton naves;

    //Botno de Salir
    protected TextButton salir;

    //Imagen nave
    protected Image naveImg;

    //muscia
    protected Music music;


    public MenuState(final GameStateManager gam, final int idConf) {
        super(gam);

        // Creem el viewport amb les mateixes dimensions que la càmera
        StretchViewport viewport = new StretchViewport(ShipAdventure.GAME_WIDTH/2,ShipAdventure.GAME_HEIGHT/2,cam);

        //Creem l'stage i assignem el viewport
        stage = new Stage(viewport);

        ShipAdventure.subirVolumen();


        //Creem el butonStyle
        buttonStyle=new TextButton.TextButtonStyle();
        buttonStyle.font= AssetManager.font;

        //Creem el boto de jugar
        jugar=new TextButton("JUGAR",buttonStyle);
        jugar.setBounds(cam.viewportWidth/2-jugar.getWidth()/2,100,jugar.getWidth(),jugar.getHeight());
        //Li donem la accio a jugar
        jugar.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AssetManager.sonidoBoton.play(0.2f);
                gam.set(new PlayState(gam,0,idConf,1));
            }
        });
        //Creem el boto de naus
        naves=new TextButton("NAVES",buttonStyle);
        naves.setBounds(cam.viewportWidth/2-naves.getWidth()/2,70,naves.getWidth(),naves.getHeight());
        //Li donem la accio a naves
        naves.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AssetManager.sonidoBoton.play(0.2f);
                gam.set(new NaveState(gam));

            }
        });
        //Creem el boto de sortir
        salir=new TextButton("SALIR",buttonStyle);
        salir.setBounds(cam.viewportWidth/2-salir.getWidth()/2,40,salir.getWidth(),salir.getHeight());
        //Li donem la accio a naves
        salir.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                AssetManager.sonidoBoton.play(0.2f);
                Gdx.app.exit();
            }
        });

        //Creem la textura del fons
        fons=AssetManager.fondoJuego;

        //Creem la imatge de la nau
        naveImg=new Image(AssetManager.nave);
        naveImg.setBounds(-100,50,Nave.NAVE_ANCHO,Nave.NAVE_ALTO);

        //Apliquem la accio a la nau de moviment horitzontal
        float y = naveImg.getImageY()+30;
        naveImg.addAction(Actions.repeat(RepeatAction.FOREVER, Actions.sequence(Actions.moveTo(0 - naveImg.getWidth(), y), Actions.moveTo(ShipAdventure.GAME_WIDTH/2, y, 5))));



        //Afegim el fons al stage
        Image fonsImage=new Image(fons);
        fonsImage.setBounds(cam.position.x-(cam.viewportWidth/2),0,cam.viewportWidth,cam.viewportHeight);
        stage.addActor(fonsImage);
        //Afegim la nau
        stage.addActor(naveImg);
        //Afegim el botons
        stage.addActor(jugar);
        stage.addActor(naves);
        stage.addActor(salir);


        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void handleInput() {

        if(Gdx.input.justTouched()){
           // gam.set(new PlayState(gam));

        }
    }

    @Override
    public void update(float dt) {
        handleInput();

    }

    @Override
    public void render(SpriteBatch sb) {

       stage.draw();
       stage.act(0.02f);


    }


}
