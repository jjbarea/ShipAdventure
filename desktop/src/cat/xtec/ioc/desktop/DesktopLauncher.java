package cat.xtec.ioc.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import cat.xtec.ioc.ShipAdventure;


public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

        config.title = "ShipAdventure";
        config.width = ShipAdventure.GAME_WIDTH;
        config.height = ShipAdventure.GAME_HEIGHT;

		new LwjglApplication(new ShipAdventure(), config);
	}
}
